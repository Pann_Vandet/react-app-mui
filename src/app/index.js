import React from 'react';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from 'material-ui/AppBar';
import Checkbox from 'material-ui/Checkbox';


class TodoComponent extends React.Component{
    render(){
        return(
            <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>

                <Checkbox
                    name="StylesOverridingInlineExample"
                    label="Checked the mail"
                    style={{
                        width: '50%',
                        margin: '0 auto',
                        border: '2px solid #FF9800',
                        backgroundColor: '#ffd699',
                    }}
                />
            </MuiThemeProvider>

        );
    }
}

export default TodoComponent
